package com.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("/")
public class OrdersServices {
	//get all orders detail
	@GET
	@Path("/orders")
	@Produces(MediaType.APPLICATION_JSON)
	public List<OrdersDto>  getAllOrderservice() {
		List<OrdersDto> rst = OrdersDao.allOrders();
		return rst;
	}
	//get one orders by id
	@GET
	@Path("/order/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public OrdersDto  getSingleOrderService(@PathParam("id") int id) {
		OrdersDto rst = OrdersDao.singleOrderById(id);
		return rst;
	}
	//create a order
	@POST
	@Path("/ordercar")
	@Consumes(MediaType.APPLICATION_JSON)
	public Response postOrderCar(OrdersDto order) throws ParseException {	
		  	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		  	Date today = sdf.parse(sdf.format(new Date()));
	        Date start = sdf.parse(order.getStartDate());
	        Date end = sdf.parse(order.getEndDate());
		
	    if(order.getStartDate()==null||order.getStartDate().length()==0){
			return Response.status(Status.BAD_REQUEST.getStatusCode())
						.entity("Start Date can not be empty").build();
		}
	    if(order.getEndDate()==null||order.getStartDate().length()==0){
			return Response.status(Status.BAD_REQUEST.getStatusCode())
						.entity("End Date can not be empty").build();
		}  
	    if(start.compareTo(today) < 0){
			return Response.status(Status.BAD_REQUEST.getStatusCode())
					.entity("Start Date can not earlier than today's date").build();
		}
		if(end.compareTo(today) < 0){
			return Response.status(Status.BAD_REQUEST.getStatusCode())
					.entity("End Date can not earlier than today's date").build();
		}
		if(start.compareTo(end) > 0 ){
			return Response.status(Status.BAD_REQUEST.getStatusCode())
					.entity("Start Date can not earlier than end date").build();
		}
		Boolean flag  = OrdersDao.validateOrderInfo(order);
	
		if(flag == false) {
			return Response.status(Status.BAD_REQUEST.getStatusCode())
					.entity("Choose A Avaliable Date range, Choose a valid CarID").build();
		}	
		return  Response.status(Status.OK.getStatusCode()).build();
	}
	  //delete a order by id
	  @DELETE
	  @Path("delete/{id}")
	  public Response deleteOrderById(@PathParam("id") int id) {
	       OrdersDao.deleteOrderById(id);
	       return Response.status(Status.OK.getStatusCode()).build();
	  }
}
