package com.test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class OrdersDao {
	   public static List<OrdersDto>  allOrders() {
		   
		   List<OrdersDto>  rst = new ArrayList<>(); 
	        try {
	        	// create db connection
	        	Connection conn = DbConnection.getInstance().getConnection();
	        	Statement stmt = conn.createStatement();
	            String sql = "SELECT * FROM orders";
	            // execute db statement
	            ResultSet rs = stmt.executeQuery(sql);
	            List<OrdersDto> orders=new ArrayList<>();
	            // get and filter data results
	          while(rs.next()){
	                int orderID  = rs.getInt("orderID");
	                int carId  = rs.getInt("carId");
	                String startDate = rs.getString("startDate");
	                String endDate = rs.getString("endDate");
	                OrdersDto order = new OrdersDto();
	                order.setOrderID(orderID);
	                order.setCarId(carId);
	                order.setStartDate(startDate);
	                order.setEndDate(endDate);
	                orders.add(order);
	           }
	          rst = orders;
	        }catch(Exception e){
	        	// deal  exception
	        	e.printStackTrace();
	        }
		   	return rst;
	   }
	   public static OrdersDto  singleOrderById(int orderId) {
		   	OrdersDto rst = new OrdersDto();
	        try {
	        	// create db connection	        	
	        	Connection conn = DbConnection.getInstance().getConnection();
	        	Statement stmt = conn.createStatement();
	            String sql = "SELECT * FROM orders where orderID = " + orderId;
	            // execute db statement
	            ResultSet rs = stmt.executeQuery(sql);
	            OrdersDto order = new OrdersDto();
	            // get and filter data results
	            rs.next();
	            int orderID  = rs.getInt("orderID");
                int carId  = rs.getInt("carId");
                String startDate = rs.getString("startDate");
                String endDate = rs.getString("endDate");
	            order.setOrderID(orderID);
                order.setCarId(carId);
                order.setStartDate(startDate);
                order.setEndDate(endDate);
	 	        rst = order;
	        }catch(Exception e){
	        	// deal  exception
	        	e.printStackTrace();
	        }
		   	return rst;
	   }
	public static Boolean validateOrderInfo(OrdersDto order) {
		List<CarModelDto>  rst = new ArrayList<>(); 
		try {
        	// create db connection        	
        	String start = order.getStartDate();
        	String end = order.getEndDate();
        	Connection conn = DbConnection.getInstance().getConnection();
        	
            String sql = "SELECT distinct rentalcar.id, "+ 
            "rentalcar.name, rentalcar.plate FROM rentalcar full join orders on " +
            "rentalcar.id = orders.carId where orders.startDate > '"+ end +"' or " +
            "'"+ start + "' > orders.endDate  or (orders.startDate IS NULL AND orders.startDate IS NULL)";
           // java.sql.PreparedStatement sqlstatement = conn.prepareStatement(sql);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            List<CarModelDto> cars=new ArrayList<>();
            Boolean flag = false;
            // get and filter data results
            while(rs.next()){
                int id  = rs.getInt("id");
                String name = rs.getString("name");
                String plateNo = rs.getString("plate");
                CarModelDto car = new CarModelDto();
                car.setCarID(id);
 	            car.setCarName(name);
 	            car.setplateNo(plateNo);
                cars.add(car);
                if(id == order.getCarId()) {
                	flag = true;
                }
           }
           rst = cars;
           if(rst.size()==0 || flag == false) {        	   
        	   return false;
           }else {
           	Statement stmt_post = conn.createStatement();
            String sql_post = "INSERT INTO orders (carId, startDate, endDate) VALUES (" +
           	"'"+order.getCarId()+"', '"+order.getStartDate()+ "', '"+order.getEndDate()+"' )";
            stmt_post.executeUpdate(sql_post);
           }
           }
            catch(Exception e){
	        	// deal  exception
	        	e.printStackTrace();
	        }
		
		return true;
	}
	public static void deleteOrderById(int id) {
		// TODO Auto-generated method stub
		 try {
	        	// create db connection
	        	Connection conn = DbConnection.getInstance().getConnection();
	        	Statement stmt = conn.createStatement();
	            String sql = "DELETE orders WHERE orderId = '"+id+"' ";
	            // execute db statement
	            stmt.executeUpdate(sql);
	        }catch(Exception e){
	        	// deal  exception
	        	e.printStackTrace();
	        }
	} 
}
