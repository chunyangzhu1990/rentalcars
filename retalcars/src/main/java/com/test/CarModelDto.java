package com.test;
// Created car model pojo
public class CarModelDto {
	
    private int carID;
    private String carName;
    private String plateNo;
    
	public int getCarID() {
		return carID;
	}
	public void setCarID(int carID) {
		this.carID = carID;
	}
	public String getCarName() {
		return carName;
	}
	public void setCarName(String carName) {
		this.carName = carName;
	}
	public String getplateNo() {
		return plateNo;
	}
	public void setplateNo(String carInStock) {
		this.plateNo = carInStock;
	}

}
