package com.test;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
//Create dao class that query and parse all data
public class CarModelDao {	   
	   public static List<CarModelDto>  carInfoWithId() {
		   
		   List<CarModelDto>  rst = new ArrayList<>(); 
	        try {
	        	// create db connection
	        	Connection conn = DbConnection.getInstance().getConnection();
	        	Statement stmt = conn.createStatement();
	            String sql = "SELECT * FROM rentalcar";
	            // execute db statement
	            ResultSet rs = stmt.executeQuery(sql);
	            List<CarModelDto> cars=new ArrayList<>();
	            // get and filter data results
	          while(rs.next()){
	                int id  = rs.getInt("id");
	                String name = rs.getString("name");
	                String plateNo = rs.getString("plate");
	                CarModelDto car = new CarModelDto();
	                car.setCarID(id);
	 	            car.setCarName(name);
	 	            car.setplateNo(plateNo);
	                cars.add(car);
	           }
	          rst = cars;
	        }catch(Exception e){
	        	// deal  exception
	        	e.printStackTrace();
	        }
		   	return rst;
	   }
	   public static CarModelDto  singleCarById(int carId) {
		   	CarModelDto rst = new CarModelDto();
	        try {
	        	// create db connection	        	
	        	Connection conn = DbConnection.getInstance().getConnection();
	        	Statement stmt = conn.createStatement();
	            String sql = "SELECT * FROM rentalcar where id = " + carId;
	            // execute db statement
	            ResultSet rs = stmt.executeQuery(sql);
                CarModelDto car = new CarModelDto();
	            // get and filter data results
	            rs.next();
	            int id  = rs.getInt("id");
	            String name = rs.getString("name");
	            String plateNo = rs.getString("plate");
	            car.setCarID(id);
	 	        car.setCarName(name);
	 	        car.setplateNo(plateNo);  
	 	        rst = car;
	        }catch(Exception e){
	        	// deal  exception
	        	e.printStackTrace();
	        }
		   	return rst;
	   }
	public static List<CarModelDto> allAvailableCars() {
		   List<CarModelDto>  rst = new ArrayList<>(); 
	        try {
	        	// create db connection
	        	Connection conn = DbConnection.getInstance().getConnection();
	        	Statement stmt = conn.createStatement();
	            String sql = "SELECT distinct rentalcar.id, rentalcar.name, rentalcar.plate FROM rentalcar full join orders on \n" + 
	            		"rentalcar.id = orders.carId where orders.startDate > CAST(CURRENT_TIMESTAMP AS DATE) or orders.startDate IS NULL";
	            // execute db statement
	            ResultSet rs = stmt.executeQuery(sql);
	            List<CarModelDto> cars=new ArrayList<>();
	            // get and filter data results
	          while(rs.next()){
	                int id  = rs.getInt("id");
	                String name = rs.getString("name");
	                String plateNo = rs.getString("plate");
	                CarModelDto car = new CarModelDto();
	                car.setCarID(id);
	 	            car.setCarName(name);
	 	            car.setplateNo(plateNo);
	                cars.add(car);
	           }
	          rst = cars;
	        }catch(Exception e){
	        	// deal  exception
	        	e.printStackTrace();
	        }
		   	return rst;
	} 
}
