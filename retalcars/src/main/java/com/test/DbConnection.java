package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
//Created Singleton JDBC class
public class DbConnection {

// database url,username and password----localDBinfo
//	private final String DB_URL = "jdbc:mysql://localhost:3306/sys?useSSL=false&serverTimezone=UTC";
//	private final String USER = "root";
//	private final String PASS = "12345678";
//	private final String DB_URL = "jdbc:sqlserver://testcar.database.windows.net:1433/test2";

	// database url,username and password----cloudDBinfo
	 String hostName = "testcar.database.windows.net"; // update me
     String dbName = "test2"; // update me
     String user = "chunyangroot"; // update me
     String password = "Mima2019"; // update me
     String url = String.format("jdbc:sqlserver://%s:1433;database=%s;user=%s;password=%s;encrypt=true;"
         + "hostNameInCertificate=*.database.windows.net;loginTimeout=30;", hostName, dbName, user, password);
     
    private static DbConnection instance;
    private Connection connection;
    private DbConnection() throws ClassNotFoundException, SQLException{
   	try {
       	// create db connection
   	      Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver"); 
          connection =  DriverManager.getConnection(url);
       }catch(SQLException se){
      // deal JDBC error
       	se.printStackTrace();
       }catch(Exception e){
      // deal other exception
        e.printStackTrace();
       }
    }  
    
    public Connection getConnection() {
        return connection;
    }
    
    public static DbConnection getInstance() throws SQLException, Exception {
        if (instance == null) {
            instance = new DbConnection();
        } else if (instance.getConnection().isClosed()) {
            instance = new DbConnection();
        }

        return instance;
    }
    public static void main(String[] args) {


    }
}