package com.test;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
@Path("/")
public class CarModelServices {
//Created Service call that contains all data	
@GET
@Path("/cars")
@Produces(MediaType.APPLICATION_JSON)
public List<CarModelDto>  getAllCarService() {
	List<CarModelDto> rst = CarModelDao.carInfoWithId();
	return rst;
}
// get car by id 
@GET
@Path("/car/{id}")
@Produces(MediaType.APPLICATION_JSON)
public CarModelDto  getSingleCarService(@PathParam("id") int id) {
	CarModelDto rst = CarModelDao.singleCarById(id);
	return rst;
}
//get cars that are available now
@GET
@Path("/availablecars")
@Produces(MediaType.APPLICATION_JSON)
public List<CarModelDto>  getAvailableCarsService() {
	List<CarModelDto> rst = CarModelDao.allAvailableCars();
	return rst;
}
}