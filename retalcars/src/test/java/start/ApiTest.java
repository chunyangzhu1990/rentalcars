/**
 * 
 */
package start;

import org.junit.BeforeClass;
import org.junit.Test;
import io.restassured.RestAssured;
import static io.restassured.RestAssured.get;

/**
 * @author Chunyang Zhu
 *
 */
public class ApiTest {
		@BeforeClass
	    public static void init() {
	        RestAssured.baseURI = "http://localhost";
	        RestAssured.port = 8080;
	    }

	    @Test
	    public void testCarInfoSuccess() {
	         get("/test-jersey-rest-maven-tomcat/rest/cars")
	        .then()
	        .statusCode(200);
	    }
	    @Test
	    public void testAvaCarSuccess() {
	         get("/test-jersey-rest-maven-tomcat/rest/availablecars")
	        .then()
	        .statusCode(200);
	    }
	    @Test
	    public void testCarByIdSuccess() {
	         get("/test-jersey-rest-maven-tomcat/rest/car/1")
	        .then()
	        .statusCode(200);
	    }
	    @Test
	    public void testOrderInfoSuccess() {
	         get("/test-jersey-rest-maven-tomcat/rest/orders")
	        .then()
	        .statusCode(200);
	    }
	    @Test
	    public void testUserAvaCarSuccess() {
	         get("/test-jersey-rest-maven-tomcat/rest/order/11")
	        .then()
	        .statusCode(200);
	    }

}
